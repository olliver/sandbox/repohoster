#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu


while getopts ":r:" _options; do
	case "${_options}" in
	r)
		_repo="${OPTARG}"
		;;
	*)
		;;
	esac
done

if [ -e "${_repo:-}" ]; then
	_repo="$(readlink -f "${_repo}")"
	OPT_DOCKER_ARGS="-v '${_repo}:${_repo}'"
fi

OPT_DOCKER_ARGS="${OPT_DOCKER_ARGS:-}" ./docker-runner.sh "./run.sh" "${@}"

exit 0
