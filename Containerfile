ARG TARGET_ARCH="library"

FROM index.docker.io/${TARGET_ARCH}/alpine:latest

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"
LABEL SPDX-License-Identifier="AGPL-3.0-or-later"

RUN \
    apk add --no-cache \
        busybox-extras \
        dumb-init \
        git \
        logrotate \
        openssh-client \
    && \
    rm -rf "/var/cache/apk/"* && \
    addgroup -S httpd && \
    adduser -D -G httpd -H -h "/var/lib/httpd" -S -s "/sbin/nologin" httpd && \
    install -D -d -m 644 "/usr/share/webapps/repohoster/" && \
    chown httpd:www-data -R "/usr/share/webapps/repohoster/"

COPY "./bin/index.cgi" "/usr/local/bin/index.cgi"
COPY "./bin/repohoster.sh" "/usr/local/sbin/repohoster.sh"
COPY "./bin/reposync.sh" "/usr/local/bin/reposync.sh"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
COPY "./dockerfiles/docker-entrypoint.sh" "/init"
COPY "./dockerfiles/logrotate.conf" "/etc/logrotate.conf"

RUN chmod go-rwx "/etc/logrotate.conf"

EXPOSE 80/tcp

HEALTHCHECK CMD wget -q -O "/dev/null" "http://localhost/" || exit 1

ENTRYPOINT [ "/init" ]
