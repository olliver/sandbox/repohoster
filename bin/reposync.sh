#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS] BASE_PATH"
	echo "Clone and/or update a repository and all of its branches into BASE_PATH"
	echo "    -h  Print usage"
	echo "    -r  Repository to sync"
}

repo_sync()
{
	_base_path="${1:?Missing argument to function}"
	_repo="${2:-}"
	_repo_path="${_base_path}/$(basename "${_repo%%.git}")"

	(
		if [ -d "${_repo_path}/.git" ]; then
			cd "${_repo_path}/.git"
			if ! git fetch --recurse-submodules --quiet; then
				echo "Failed to synchronize repository, continuing"
			fi
		elif [ -n "${_repo:-}" ]; then
			if ! git clone \
			         --bare \
			         --depth 1 \
			         --no-single-branch \
			         --recurse-submodules \
			         --shallow-submodules \
			         "${_repo}" "${_repo_path}/.git"; then
				echo "Failed to clone, not a git repository?"
				exit 1
			fi
		fi

		cd "${_repo_path}"
		eval "$(git for-each-ref \
			    --shell \
			    --format "test -d '${_repo_path}/%(refname:short)' &&
			              rm -r '${_repo_path}/%(refname:short)';
			              git worktree add --force '${_repo_path}/%(refname:short)' '%(refname)'" \
			    "refs/heads/")"
	)
}

main()
{
	while getopts ":hr:" _options; do
	case "${_options}" in
	h)
		usage
		exit 0
		;;
	r)
		_repo="${OPTARG}"
		if [ -e "${_repo}" ]; then
			_repo="file://${_repo}"
		fi
		;;
	:)
		e_err "Option -${OPTARG} requires an argument."
		exit 1
		;;
	?)
		e_err "Invalid option: -${OPTARG}"
		exit 1
		;;
	*)
		;; # fall-through
	esac
	done
	shift "$((OPTIND - 1))"

	if [ -z "${1:-}" ]; then
		echo "Missing repository path"
		usage
		exit 1
	fi

	repo_sync "${1}" "${_repo:-}"
}

main "${@}"

exit 0
