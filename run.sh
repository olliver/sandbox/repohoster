#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_WWW_ROOT="/usr/share/webapps/repohoster"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo "Clone package repository and host it"
	echo "    -b  Base path to serve (default: '${DEF_WWW_ROOT}') [WWW_ROOT]"
	echo "    -f  Force local directory (try not to git clone)"
	echo "    -h  Print usage"
	echo "    -r  Location of the package repository [PACKAGE_REPOSITORY]"
	echo
	echo "All options can also be passed in environment variables (listed between [brackets])."
	echo "Warning: Hosting a local directory will try to create a cgi-bin/index.cgi in the directory."
}

main()
{
	_start_time="$(date "+%s")"

	while getopts ":b:fhr:" _options; do
		case "${_options}" in
		b)
			www_root="${OPTARG}"
			;;
		f)
			force="true"
			;;
		h)
			usage
			exit 0
			;;
		r)
			PACKAGE_REPOSITORY="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		*)
			;; # fall-through
		esac
	done
	shift "$((OPTIND - 1))"

	www_root="${www_root:-${WWW_ROOT:-${DEF_WWW_ROOT}}}"

	./buildenv_check.sh

	if [ -z "${PACKAGE_REPOSITORY:-}" ]; then
		e_warn "Required parameter 'PACKAGE_REPOSITORY' not set, running with pwd"
		PACKAGE_REPOSITORY="file://$(pwd)"
	fi

	if [ "${force:-}" = "true" ] || \
	   ! ./bin/reposync.sh -r "${PACKAGE_REPOSITORY}" "${www_root}"; then
		www_root="${PACKAGE_REPOSITORY}"
	fi
	WWW_ROOT="${www_root}" ./bin/repohoster.sh

	echo "Ran application for $(($(date "+%s") - _start_time)) seconds"
}

main "${@}"

exit 0
